package ca.ryerson.scs.cscu;


import spark.Spark;

/**
 * The main class of the application.
 * Contains all the routes for the application.
 */
public class Routes {
	public static void main(String[] args) {
		Spark.get("/", (request, response) -> "Hello World!");
	}
}
